package com.honestgoods.mybucketlist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.honestgoods.mybucketlist.item.BucketItem;

import java.util.ArrayList;

/**
 * Created by Hong on 2016-12-22.
 */

public class SQliteHelper extends SQLiteOpenHelper {
    public static String databaseName = "bucket.db";
    private static SQliteHelper instance;
    public static SQliteHelper getInstance(Context context) {
        if(instance==null) instance = new SQliteHelper(context, databaseName, null, 6);
        return instance;
    }

    public SQliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE bucket ( num INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT, grade int, limits TEXT, state int, sticker INTEGER, category INTEGER);");
        sqLiteDatabase.execSQL("CREATE TABLE stickers ( num INTEGER PRIMARY KEY NOT NULL, enabled INTEGER );");
        for(int i=0; i<StickerManager.stickerArray.length; i++) {
            if(i<3)
                sqLiteDatabase.execSQL("INSERT INTO stickers VALUES ( ?, 1 );", new Object[] { i });
            else
                sqLiteDatabase.execSQL("INSERT INTO stickers VALUES ( ?, 0 );", new Object[] { i });
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        HGUtils.log("onUpgrade. old:" + oldVersion +", new:"+newVersion);
        switch(oldVersion) {
            case 1:
                sqLiteDatabase.execSQL("ALTER TABLE bucket ADD sticker INTEGER;");
            case 2:
                sqLiteDatabase.execSQL("CREATE TABLE stickers ( num INTEGER PRIMARY KEY NOT NULL, enabled INTEGER );");
            case 3:
            case 4:
            case 5:
            case 6:
                sqLiteDatabase.execSQL("ALTER TABLE bucket ADD category INTEGER;");
                sqLiteDatabase.execSQL("UPDATE bucket SET category=0;");
        }
    }

    public void updateStickers() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM stickers;", null);
        cursor.moveToNext();
        int stickers = cursor.getInt(0);
        cursor.close();
        db = getWritableDatabase();
        for(int i=stickers; i<StickerManager.stickerArray.length; i++) {
            if(i<3)
                db.execSQL("INSERT INTO stickers VALUES ( ?, 1 );", new Object[] { i });
            else
                db.execSQL("INSERT INTO stickers VALUES ( ?, 0 );", new Object[] { i });
        }
    }

    public void insert(BucketItem item) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO bucket VALUES ( null, ?, ?, ?, 0, ?, ?);", new Object[] { item.getTitle(), item.getGrade(), item.getLimit(), item.getSticker(), item.getCategory() });
    }

    public void update(BucketItem item) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE bucket SET title=?, grade=?, limits=?, sticker=?, category=? WHERE num=?;", new Object[] { item.getTitle(), item.getGrade(), item.getLimit(), item.getSticker(), item.getCategory(), item.getNum()});
    }

    public void delete(int num) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM bucket WHERE num=?;", new Object[] { num });
    }

    public void setSuccess(int num) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE bucket SET state=1 WHERE num=?;", new Object[] {num});
    }

    public void setProceed(int num) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("UPDATE bucket SET state=0 WHERE num=?;", new Object[] { num });
    }

    public ArrayList<BucketItem> selectAll() {
        ArrayList<BucketItem> ret = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM bucket ORDER BY grade DESC;", null);
        while(cursor.moveToNext()) {
            BucketItem item = new BucketItem();
            item.setNum(cursor.getInt(0));
            item.setTitle(cursor.getString(1));
            item.setGrade(cursor.getString(2));
            item.setLimit(cursor.getString(3));
            item.setState(cursor.getInt(4));
            item.setSticker(cursor.getInt(5));
            ret.add(item);
        }
        cursor.close();
        return ret;
    }

    public ArrayList<BucketItem> selectAll(int category) {
        ArrayList<BucketItem> ret = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM bucket WHERE category=? ORDER BY grade DESC;", new String[] { category + "" });
        while(cursor.moveToNext()) {
            BucketItem item = new BucketItem();
            item.setNum(cursor.getInt(0));
            item.setTitle(cursor.getString(1));
            item.setGrade(cursor.getString(2));
            item.setLimit(cursor.getString(3));
            item.setState(cursor.getInt(4));
            item.setSticker(cursor.getInt(5));
            ret.add(item);
        }
        cursor.close();
        return ret;
    }

    public ArrayList<BucketItem> selectProceeding(int category) {
        ArrayList<BucketItem> ret = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM bucket WHERE state=0 AND category=? ORDER BY grade DESC;", new String[] { category + "" });
        while(cursor.moveToNext()) {
            BucketItem item = new BucketItem();
            item.setNum(cursor.getInt(0));
            item.setTitle(cursor.getString(1));
            item.setGrade(cursor.getString(2));
            item.setLimit(cursor.getString(3));
            item.setState(cursor.getInt(4));
            item.setSticker(cursor.getInt(5));
            ret.add(item);
        }
        cursor.close();
        return ret;
    }

    public ArrayList<BucketItem> selectComplete(int category) {
        ArrayList<BucketItem> ret = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM bucket WHERE state=1 AND category=? ORDER BY grade DESC;", new String[] { category + "" });
        while(cursor.moveToNext()) {
            BucketItem item = new BucketItem();
            item.setNum(cursor.getInt(0));
            item.setTitle(cursor.getString(1));
            item.setGrade(cursor.getString(2));
            item.setLimit(cursor.getString(3));
            item.setState(cursor.getInt(4));
            ret.add(item);
        }
        cursor.close();
        return ret;
    }

    public BucketItem selectRandom() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM bucket ORDER BY RANDOM() LIMIT 1;", null);
        BucketItem item = null;
        if(cursor.moveToNext()) {
            item = new BucketItem();
            item.setNum(cursor.getInt(0));
            item.setTitle(cursor.getString(1));
            item.setGrade(cursor.getString(2));
            item.setLimit(cursor.getString(3));
            item.setState(cursor.getInt(4));
        }
        return item;
    }

    public BucketItem selectNum(int num) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM bucket WHERE num=?;", new String[] { num + "" });
        BucketItem item = null;
        if(cursor.moveToNext()) {
            item = new BucketItem();
            item.setNum(cursor.getInt(0));
            item.setTitle(cursor.getString(1));
            item.setGrade(cursor.getString(2));
            item.setLimit(cursor.getString(3));
            item.setState(cursor.getInt(4));
        }
        cursor.close();
        return item;
    }

    public boolean[] getStickersEnabled(StickerManager stickerManager) {
        int stickerCount = stickerManager.getStickerCount();
        boolean[] ret = new boolean[stickerCount];
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM stickers;", null);
        int count = 0;
        while(cursor.moveToNext()) {
            ret[count++] = cursor.getInt(1) == 1;
        }
        return ret;
    }

    public void setStickerEnabled(int index, boolean enabled) {
        HGUtils.log("setStickerEnabled " + index + " " + enabled);
        SQLiteDatabase db = getWritableDatabase();
        int _enabled = 0;
        if(enabled) _enabled = 1;
        db.execSQL("UPDATE stickers SET enabled=? WHERE num=?;", new Object[] { _enabled, index });
    }
}
