package com.honestgoods.mybucketlist.adapter;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.honestgoods.mybucketlist.HGUtils;
import com.honestgoods.mybucketlist.R;
import com.honestgoods.mybucketlist.SQliteHelper;

/**
 * Created by Hong on 2017-02-26.
 */

public class MainPagerAdapter extends PagerAdapter {
    private Context context;
    private FloatingActionButton fabCreate;
    private int shown;

    public MainPagerAdapter(Context context, FloatingActionButton fab, int shown) {
        this.context = context;
        fabCreate = fab;
        this.shown = shown;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        HGUtils.log(container.getChildCount() + "");
        if(container.getChildAt(position) != null) {
            container.getChildAt(position).setVisibility(View.VISIBLE);
            return container.getChildAt(position);
        }
        View contentView = LayoutInflater.from(context).inflate(R.layout.item_view_pager_list, container, false);
        RecyclerView recyclerView = (RecyclerView)contentView.findViewById(R.id.main_recycle);
        MainRecyclerAdapter adapter;
        switch(shown) {
            case 1: adapter = new MainRecyclerAdapter(context, SQliteHelper.getInstance(context).selectProceeding(position)); break;
            case 2: adapter = new MainRecyclerAdapter(context, SQliteHelper.getInstance(context).selectComplete(position)); break;
            default: adapter = new MainRecyclerAdapter(context, SQliteHelper.getInstance(context).selectAll(position)); break;
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(onScrollListener);
        container.addView(contentView);
        return contentView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        try {
            container.getChildAt(position).setVisibility(View.GONE);
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
//            if(newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//                fabCreate.animate().translationY(0);
//                fabCreate.animate().alpha(1f);
//            } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
//                fabCreate.animate().translationY(-150);
//                fabCreate.animate().alpha(0.5f);
//            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if(dy > 0) {
                fabCreate.animate().translationY(-150);
            } else if(dy < 0) {
                fabCreate.animate().translationY(0);
            }
        }
    };
}
