package com.honestgoods.mybucketlist.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.honestgoods.mybucketlist.R;

/**
 * Created by Hong on 2017-01-26.
 */

public class CategorySpinnerAdapter implements SpinnerAdapter {
    private Context context;
    private String[] data;

    public CategorySpinnerAdapter(Context context, String[] data) {
        this.context = context;
        this.data = data;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if(view == null) view = LayoutInflater.from(context).inflate(R.layout.item_spinner_notify, viewGroup, false);
        TextView textTitle = (TextView)view.findViewById(R.id.text_title);
        textTitle.setText(data[i]);
        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int i) {
        return data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null) view = LayoutInflater.from(context).inflate(R.layout.item_spinner_notify, viewGroup, false);
        TextView textTitle = (TextView)view.findViewById(R.id.text_title);
        textTitle.setText(data[i]);
        return view;
    }

    @Override
    public int getItemViewType(int i) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return data.length == 0;
    }
}
