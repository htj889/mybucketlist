package com.honestgoods.mybucketlist.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.item.BucketItem;
import com.honestgoods.mybucketlist.HGUtils;
import com.honestgoods.mybucketlist.HongAnimationUtil;
import com.honestgoods.mybucketlist.R;
import com.honestgoods.mybucketlist.SQliteHelper;
import com.honestgoods.mybucketlist.StickerManager;
import com.honestgoods.mybucketlist.activity.MainActivity;

import java.util.ArrayList;

/**
 * Created by Hong on 2016-12-21.
 */

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder> {
    public static int defaultHeight = 0;
    private ViewHolder expandedViewHolder;
    private Context context;
    private ArrayList<BucketItem> data;
    private int editingItemNumber;

    public MainRecyclerAdapter(Context context, ArrayList<BucketItem> data) {
        this.context = context;
        this.data = data;
        editingItemNumber = -1;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_bucket, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        BucketItem item = data.get(position);
        if(item.getNum() == 0) {
            holder.linear.setVisibility(View.GONE);
            holder.cardView.setClickable(false);
        } else {
            holder.linear.setVisibility(View.VISIBLE);
            holder.cardView.setClickable(true);
            holder.setItem(item);
            holder.setForeground();
            if (editingItemNumber == item.getNum()) {
                holder.setLongView();
                holder.isExpanded = true;
                expandedViewHolder = holder;
            } else {
                holder.setShortView();
                holder.isExpanded = false;
            }
        }
        holder.cardView.getLayoutParams().height = FrameLayout.LayoutParams.WRAP_CONTENT;
    }

    public void expandView(ViewHolder holder) {
        if(defaultHeight == 0) defaultHeight = holder.cardView.getHeight();
        if(expandedViewHolder!=null) {
            reduceView(expandedViewHolder);
        }
        holder.setLongView();
        holder.isExpanded = true;
        HongAnimationUtil.expandView(holder.cardView, 300, holder.cardView.getWidth(), defaultHeight);
        expandedViewHolder = holder;
    }

    public void reduceView(final ViewHolder holder) {
        holder.isExpanded = false;
        HongAnimationUtil.collapseView(holder.cardView, 300, defaultHeight, false, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                holder.setShortView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        expandedViewHolder = null;
    }

    public boolean haveExpandedView() {
        return expandedViewHolder != null;
    }
    public void reduceExpandedView() {
        if(expandedViewHolder != null) reduceView(expandedViewHolder);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public static final int STATE_PROCEEDING = 0;
        public static final int STATE_COMPLETE = 1;
        public static final int STATE_FAIL = 2;
        private TextView title, grade, limit, state;
        private LinearLayout more1, more2;
        private Button success, delete, edit;
        private CardView cardView;
        private boolean isExpanded;
        private BucketItem item;
        private ImageView sticker;
        private LinearLayout linear;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.item_main_bucket_title);
            grade = (TextView)itemView.findViewById(R.id.item_main_bucket_grade);
            state = (TextView)itemView.findViewById(R.id.item_main_bucket_state);
            limit = (TextView)itemView.findViewById(R.id.item_main_bucket_limit);
            more1 = (LinearLayout)itemView.findViewById(R.id.item_main_bucket_more1);
            more2 = (LinearLayout)itemView.findViewById(R.id.item_main_bucket_more2);
            success = (Button)itemView.findViewById(R.id.item_main_bucket_success);
            delete = (Button)itemView.findViewById(R.id.item_main_bucket_delete);
            edit = (Button)itemView.findViewById(R.id.item_main_bucket_edit);
            cardView = (CardView)itemView.findViewById(R.id.item_main_bucket_card);
            sticker = (ImageView)itemView.findViewById(R.id.item_main_bucket_stiker);
            linear = (LinearLayout)itemView.findViewById(R.id.item_main_bucket_linear);
            isExpanded = false;

            cardView.setOnClickListener(this);
            success.setOnClickListener(this);
            edit.setOnClickListener(this);
            delete.setOnClickListener(this);
        }

        public void setItem(BucketItem item) {
            this.item = item;
        }

        private void setState(int s) {
            switch(s) {
                case STATE_PROCEEDING:
                    state.setText(context.getString(R.string.item_state_proceeding));
                    state.setTextColor(ActivityCompat.getColor(context, R.color.fontBlue));
                    success.setText(context.getString(R.string.item_button_complete));
                    edit.setVisibility(View.VISIBLE);
                    break;
                case STATE_COMPLETE:
                    state.setText(context.getString(R.string.item_state_complete));
                    state.setTextColor(ActivityCompat.getColor(context, R.color.fontGreen));
                    success.setText(context.getString(R.string.item_button_revert));
                    edit.setVisibility(View.GONE);
                    break;
                case STATE_FAIL:
                    state.setText(context.getString(R.string.item_state_fail));
                    state.setTextColor(ActivityCompat.getColor(context, R.color.fontOrange));
                    edit.setVisibility(View.VISIBLE);
                    break;
            }
            int length = state.getText().toString().length();
            if(length > 7) {
                state.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.font_smallest));
            }
            else if (length > 5) {
                state.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.font_small));
            }
            else {
                state.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.font_medium));
            }
        }

        private void setGrade() {
            grade.setText(item.getGrade());
            int _grade = 0;
            if(!item.getGrade().equals("")) _grade = Integer.parseInt(item.getGrade());
            if(_grade > 75) {
                grade.setTextColor(ContextCompat.getColor(context, R.color.fontOrange));
            } else if (_grade > 50) {
                grade.setTextColor(ContextCompat.getColor(context, R.color.fontBlue));
            } else if (_grade > 25) {
                grade.setTextColor(ContextCompat.getColor(context, R.color.fontGreen));
            } else {
                grade.setTextColor(ContextCompat.getColor(context, R.color.fontGrey));
            }
        }

        public void setForeground() {
            if(item == null) return;
            setGrade();
            setState(item.getState());
            if(item.getSticker()!=-1) {
                sticker.setImageResource(StickerManager.stickerArray[item.getSticker()]);
            } else {
                sticker.setImageResource(0);
            }
            if(item.getLimit().equals("")) {
                limit.setVisibility(View.GONE);
            } else {
                limit.setVisibility(View.VISIBLE);
                limit.setText(item.getLimit());
            }
        }

        public void setShortView() {
            title.setMaxLines(1);
            title.setText(HGUtils.cutStringByLength(item.getTitle(), 26, "..."));
            more1.setVisibility(View.GONE);
            more2.setVisibility(View.GONE);
        }

        public void setLongView() {
            title.setMaxLines(5);
            title.setText(item.getTitle());
            more1.setVisibility(View.VISIBLE);
            more2.setVisibility(View.VISIBLE);
        }

        private void removeItemFromAdapter(int position) {
            data.remove(position);
            notifyItemRemoved(position);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            }, 400);
        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            if(view.equals(cardView)) {
                if(isExpanded) {
                    reduceView(this);
                } else {
                    expandView(this);
                }
            } else if(view.equals(delete)) {
                new AlertDialog.Builder(context)
                        .setMessage(R.string.text_warn_delete)
                        .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                SQliteHelper.getInstance(context).delete(data.get(position).getNum());
                                removeItemFromAdapter(position);
                            }
                        })
                        .setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            } else if (view.equals(success)) {
                String strState = state.getText().toString();
                if(strState.equals(context.getString(R.string.item_state_proceeding))) {
                    setState(STATE_COMPLETE);
                    item.setState(STATE_COMPLETE);
                    SQliteHelper.getInstance(context).setSuccess(data.get(position).getNum());
                } else if(strState.equals(context.getString(R.string.item_state_complete))) {
                    setState(STATE_PROCEEDING);
                    item.setState(STATE_PROCEEDING);
                    SQliteHelper.getInstance(context).setProceed(data.get(position).getNum());
                }
            } else if (view.equals(edit)) {
                editingItemNumber = item.getNum();

                MainActivity mainActivity = (MainActivity)context;
                mainActivity.hideList(data.get(position));
            }
        }
    }
}
