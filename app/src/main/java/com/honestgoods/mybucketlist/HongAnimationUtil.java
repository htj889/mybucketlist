package com.honestgoods.mybucketlist;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.FrameLayout;

/**
 * Created by Hong on 2016-12-22.
 */

public class HongAnimationUtil {
    public static void disappearView(final View v) {
        Animation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration(300);
        animation.setInterpolator(new AccelerateInterpolator());
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        v.startAnimation(animation);
    }

    public static void appearView(final View v) {
        v.setVisibility(View.VISIBLE);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(300);
        animation.setInterpolator(new AccelerateInterpolator());
        v.startAnimation(animation);
    }

    public static void expandView(final View v, int duration, int width, final int startHeight) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(widthMeasureSpec, heightMeasureSpec);
        final int expandHeight = v.getMeasuredHeight() - startHeight;

        v.getLayoutParams().height = startHeight;
        v.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime != 1) {
                    v.getLayoutParams().height = startHeight + (int)(expandHeight * interpolatedTime);
                } else {
                    v.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                }
                v.requestLayout();
            }
        };
        animation.setDuration(duration);
        animation.setInterpolator(new DecelerateInterpolator());
        v.startAnimation(animation);
    }

    public static void collapseView(final View v, int duration, final int endHeight, final boolean gone, @Nullable Animation.AnimationListener listener) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(v.getWidth(), View.MeasureSpec.EXACTLY);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(widthMeasureSpec, heightMeasureSpec);
        final int collapseHeight = v.getMeasuredHeight() - endHeight;

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime != 1) {
                    v.getLayoutParams().height = endHeight + (int)(collapseHeight * (1 - interpolatedTime));
                } else {
                    v.getLayoutParams().height = endHeight;
                    if(gone) v.setVisibility(View.GONE);
                }
                v.requestLayout();
            }
        };
        animation.setDuration(duration);
        animation.setInterpolator(new AccelerateInterpolator());
        if(listener!=null) animation.setAnimationListener(listener);
        v.startAnimation(animation);
    }
}
