package com.honestgoods.mybucketlist;

import android.content.Context;

/**
 * Created by Hong on 2016-12-31.
 */

public class StickerManager {
    public static final int[] stickerArray = new int[] {
            R.drawable.sticker_0,
            R.drawable.sticker_1,
            R.drawable.sticker_2,
            R.drawable.sticker_3,
            R.drawable.sticker_4,
            R.drawable.sticker_5,
            R.drawable.sticker_6,
            R.drawable.sticker_7,
            R.drawable.sticker_8,
            R.drawable.sticker_9,
            R.drawable.sticker_10,
            R.drawable.sticker_11,
            R.drawable.sticker_12,
            R.drawable.sticker_13,
            R.drawable.sticker_14,
            R.drawable.sticker_15,
            R.drawable.sticker_16,
            R.drawable.sticker_17,
            R.drawable.sticker_18,
            R.drawable.sticker_19,
            R.drawable.sticker_20,
            R.drawable.sticker_21,
            R.drawable.sticker_22,
            R.drawable.sticker_23,
            R.drawable.sticker_24,
            R.drawable.sticker_25,
            R.drawable.sticker_26,
            R.drawable.sticker_27,
            R.drawable.sticker_28,
            R.drawable.sticker_29,
            R.drawable.sticker_30,
            R.drawable.sticker_31,
            R.drawable.sticker_32,
            R.drawable.sticker_33,
            R.drawable.sticker_34,
            R.drawable.sticker_35,
            R.drawable.sticker_36,
            R.drawable.sticker_37,
            R.drawable.sticker_38,
            R.drawable.sticker_39};
    private boolean[] enabled;
    private Context context;

    public StickerManager(Context context) {
        this.context = context;
        enabled = SQliteHelper.getInstance(context).getStickersEnabled(this);
        SQliteHelper.getInstance(context).updateStickers();
    }

    public int getStickerId(int index) { return stickerArray[index]; }
    public int getStickerCount() { return stickerArray.length; }

    public boolean getEnabled(int index) { return enabled[index]; }
    public void setEnabled(int index, boolean enabled) {
        SQliteHelper.getInstance(context).setStickerEnabled(index, enabled);
        this.enabled[index] = enabled;
    }
}
