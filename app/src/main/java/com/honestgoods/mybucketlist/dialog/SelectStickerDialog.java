package com.honestgoods.mybucketlist.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.HGUtils;
import com.honestgoods.mybucketlist.R;
import com.honestgoods.mybucketlist.StickerManager;

/**
 * Created by Hong on 2016-12-29.
 */

public class SelectStickerDialog extends Dialog {
    private LinearLayout[] stickerListViews;
    private OnCompleteListener onClickListener;
    private int selectedIndex;
    private StickerManager stickerManager;
    private int rewardedIndex;
    private InterstitialAd interstitialAd;

    public SelectStickerDialog(Context context, int defaultIndex, OnCompleteListener onClickListener) {
        super(context);
        this.onClickListener = onClickListener;
        selectedIndex = defaultIndex;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_sticker);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
        initInterstitial();
        requestNewInterstitial();

        stickerListViews = new LinearLayout[3];
        stickerListViews[0] = (LinearLayout)findViewById(R.id.dialog_select_sticker_list1);
        stickerListViews[1] = (LinearLayout)findViewById(R.id.dialog_select_sticker_list2);
        stickerListViews[2] = (LinearLayout)findViewById(R.id.dialog_select_sticker_list3);

        stickerManager = new StickerManager(getContext());
        showStickers();

        findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        findViewById(R.id.button_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onComplete(SelectStickerDialog.this, selectedIndex);
            }
        });
        findViewById(R.id.button_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedIndex == -1) return;
                ImageView imageView;
                imageView = (ImageView)stickerListViews[selectedIndex%3].getChildAt(selectedIndex/3);
                imageView.clearColorFilter();
                selectedIndex = -1;
            }
        });
    }

    private void initInterstitial() {
        interstitialAd = new InterstitialAd(getContext());
        interstitialAd.setAdUnitId(getContext().getString(R.string.ad_full_unit_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                HGUtils.log(rewardedIndex + " is opened");
                stickerManager.setEnabled(rewardedIndex, true);
                setImageClear(rewardedIndex);
                rewardedIndex = -1;
                requestNewInterstitial();
            }
        });
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("5174D811535887316CA78B671966851D")
                .build();
        interstitialAd.loadAd(adRequest);
    }

    private void showInterstitial() {
        if(interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

    private void showStickers() {
        int stickerSize = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45, getContext().getResources().getDisplayMetrics());
        int stickerMargine = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getContext().getResources().getDisplayMetrics());
        boolean noAds = HGSharedPreferences.getInstance(getContext()).haveNoAds();
        for(int i=0; i<stickerManager.getStickerCount(); i++) {
            ImageView sticker = new ImageView(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(stickerSize, stickerSize);
            params.topMargin = stickerMargine;
            params.bottomMargin = stickerMargine;
            params.leftMargin = stickerMargine;
            params.rightMargin = stickerMargine;
            sticker.setLayoutParams(params);
            sticker.setImageResource(stickerManager.getStickerId(i));
            sticker.setTag(Integer.valueOf(i));
            sticker.setOnClickListener(onStickerClickListener);
            stickerListViews[i%3].addView(sticker);
            if(!stickerManager.getEnabled(i) && !noAds) sticker.setColorFilter(0x40ff0000, PorterDuff.Mode.OVERLAY);
            else if(selectedIndex == i) setImageSelected(i);
        }
    }

    public interface OnCompleteListener {
        void onComplete(SelectStickerDialog dialog, int selected);
    }

    private View.OnClickListener onStickerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int number = (Integer)view.getTag();
            // 아직 오픈하지 못한 스티커일 경우
            if(!stickerManager.getEnabled(number) && !HGSharedPreferences.getInstance(getContext()).haveNoAds()) {
                rewardedIndex = number;
                new AlertDialog.Builder(getContext())
                        .setMessage(R.string.dialog_select_sticker_warning)
                        .setPositiveButton(getContext().getString(R.string.button_yes), new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                showInterstitial();
                            }
                        })
                        .setNegativeButton(getContext().getString(R.string.button_no), new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                rewardedIndex = -1;
                                dialogInterface.dismiss();
                            }
                        }).show();
                return;
            }
            if(selectedIndex != -1) {
                setImageClear(selectedIndex);
                if(selectedIndex == number) {
                    selectedIndex = -1;
                    return;
                }
            }
            setImageSelected(number);
            selectedIndex = number;
        }
    };

    private void setImageClear(int index) {
        // 선택 취소 효과
        ImageView imageView;
        imageView = (ImageView)stickerListViews[index%3].getChildAt(index/3);
        imageView.clearColorFilter();
    }

    private void setImageSelected(int index) {
        // 선택 효과
        ImageView imageView;
        imageView = (ImageView)stickerListViews[index%3].getChildAt(index/3);
        imageView.setColorFilter(0x40000000, PorterDuff.Mode.OVERLAY);
    }
}
