package com.honestgoods.mybucketlist.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.honestgoods.mybucketlist.R;

/**
 * Created by Hong on 2016-12-29.
 */

public class SimpleInputDialog extends Dialog {
    private String title;
    private EditText editText;
    private OnCompleteListener onClickListener;

    public SimpleInputDialog(Context context, String title, OnCompleteListener onClickListener) {
        super(context);
        this.title = title;
        this.onClickListener = onClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_simple_input);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);

        TextView textTitle = (TextView)findViewById(R.id.dialog_simple_input_title);
        textTitle.setText(title);
        editText = (EditText)findViewById(R.id.dialog_simple_input_edit);
        findViewById(R.id.dialog_simple_button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        findViewById(R.id.dialog_simple_button_complete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.getText().toString().equals("")) {
                    Toast.makeText(getContext(), getContext().getString(R.string.dialog_simple_input_error), Toast.LENGTH_SHORT).show();
                    editText.requestFocus();
                    return;
                }
                onClickListener.onComplete(SimpleInputDialog.this, editText.getText().toString());
            }
        });
    }

    public interface OnCompleteListener {
        void onComplete(SimpleInputDialog dialog, String input);
    }
}
