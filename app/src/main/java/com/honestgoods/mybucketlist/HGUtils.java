package com.honestgoods.mybucketlist;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.honestgoods.mybucketlist.activity.SplashActivity;
import com.honestgoods.mybucketlist.item.BucketItem;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Hong on 2016-12-27.
 */

public class HGUtils {
    public static final int NOTIFICATION_NOTICE = 100;

    public static String cutStringByLength(String string, int length, String appendString) {
        int realLength = 0;
        for(int i=0; i<string.length(); i++) {
            if(string.substring(i,i+1).matches("[ㄱ-힣]")) realLength += 2;
            else realLength++;
            if(realLength > length) {
                return string.substring(0, i) + appendString;
            }
        }
        return string;
    }

    public static void log(String log) {
        Log.v("HG_TAG", log);
    }

    public static void showNotification(Context context, BucketItem item) {
        if(item == null) {
            HGUtils.log("selectRandom. item is null");
            return;
        }
        NotificationManager manager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(item.getTitle());
        builder.setContentText(item.getLimit());
        builder.setSmallIcon(R.drawable.ic_notify);
        builder.setOngoing(true);
        builder.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, SplashActivity.class), PendingIntent.FLAG_ONE_SHOT));
        manager.notify(NOTIFICATION_NOTICE, builder.build());
    }

    public static void cancelNotification(Context context) {
        NotificationManager manager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(NOTIFICATION_NOTICE);
    }
}
