package com.honestgoods.mybucketlist.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.R;

public class PasswordActivity extends AppCompatActivity implements View.OnClickListener{
    private int mode;
    private int passwordCount;
    private int confirmCount;
    private String passwords;
    private String prePasswords;

    private Button btnCancel, btnOK;
    private TextView[] txtNumbers;
    private TextView[] txtPasswords;
    private TextView txtError, txtTitle;
    private ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        txtTitle = (TextView)findViewById(R.id.password_title);
        btnCancel = (Button)findViewById(R.id.button_cancel);
        btnOK = (Button)findViewById(R.id.button_ok);
        txtNumbers = new TextView[10];
        txtNumbers[0] = (TextView)findViewById(R.id.password_button_0);
        txtNumbers[1] = (TextView)findViewById(R.id.password_button_1);
        txtNumbers[2] = (TextView)findViewById(R.id.password_button_2);
        txtNumbers[3] = (TextView)findViewById(R.id.password_button_3);
        txtNumbers[4] = (TextView)findViewById(R.id.password_button_4);
        txtNumbers[5] = (TextView)findViewById(R.id.password_button_5);
        txtNumbers[6] = (TextView)findViewById(R.id.password_button_6);
        txtNumbers[7] = (TextView)findViewById(R.id.password_button_7);
        txtNumbers[8] = (TextView)findViewById(R.id.password_button_8);
        txtNumbers[9] = (TextView)findViewById(R.id.password_button_9);
        imgBack = (ImageView)findViewById(R.id.password_button_back);
        txtPasswords = new TextView[4];
        txtPasswords[0] = (TextView)findViewById(R.id.password_1);
        txtPasswords[1] = (TextView)findViewById(R.id.password_2);
        txtPasswords[2] = (TextView)findViewById(R.id.password_3);
        txtPasswords[3] = (TextView)findViewById(R.id.password_4);
        txtError = (TextView)findViewById(R.id.password_error);

        btnCancel.setOnClickListener(this);
        btnOK.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        for(int i=0; i<10; i++) {
            txtNumbers[i].setTag(Integer.valueOf(i));
            txtNumbers[i].setOnClickListener(onPasswordClickListener);
        }

        mode = getIntent().getIntExtra("mode", -1);
        switch(mode) {
            case 0:
                txtTitle.setText(R.string.password_set_title);
                break;
            case 1:
                txtTitle.setText(R.string.password_change_title_1);
                break;
            case 2:
            case 3:
                txtTitle.setText(R.string.password_confirm_title);
                btnCancel.setVisibility(View.GONE);
                btnOK.setVisibility(View.GONE);
                break;
        }

        passwordCount = 0;
        confirmCount = 0;
        passwords = "";
    }

    @Override
    public void onClick(View view) {
        String password = HGSharedPreferences.getInstance(this).get("password");
        switch(view.getId()) {
            case R.id.button_ok:
                if(passwordCount < 4) {
                    // 비밀번호 부족
                    txtError.setText(R.string.password_error_4);
                } else {
                    switch(mode) {
                        case 0: // 비밀번호 설정
                            if(confirmCount < 1) {
                                confirmCount++;
                                txtTitle.setText(R.string.password_enter_again);
                                prePasswords = "" + passwords;
                                clearPassword(true);
                            }
                            else {
                                if(prePasswords.equals(passwords)) { // 비밀번호가 일치
                                    HGSharedPreferences.getInstance(this).put("password", passwords);
                                    setResult(RESULT_OK);
                                    finish();
                                } else { // 비밀번호가 다름
                                    txtError.setText(R.string.password_enter_different);
                                    clearPassword(false);
                                }
                            }
                            break;
                        case 1: // 비밀번호 수정
                            if(confirmCount == 0) {
                                if(password.equals(passwords)) {
                                    confirmCount++;
                                    txtTitle.setText(R.string.password_enter_new);
                                    clearPassword(true);
                                } else {
                                    txtError.setText(R.string.password_invalid_password);
                                    clearPassword(false);
                                }
                            } else if(confirmCount == 1) {
                                confirmCount++;
                                txtTitle.setText(R.string.password_enter_again);
                                prePasswords = "" + passwords;
                                clearPassword(true);
                            } else if(confirmCount == 2) {
                                if(prePasswords.equals(passwords)) { // 비밀번호가 일치
                                    HGSharedPreferences.getInstance(this).put("password", passwords);
                                    setResult(RESULT_OK);
                                    finish();
                                } else { // 비밀번호가 다름
                                    txtError.setText(R.string.password_enter_different);
                                    clearPassword(false);
                                }
                            }
                            break;
                    }
                }
                break;
            case R.id.button_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.password_button_back:
                if(passwordCount > 0) {
                    passwordCount--;
                    passwords = passwords.substring(0, passwordCount);
                    txtPasswords[passwordCount].setText("_");
                }
                break;
        }
    }

    private View.OnClickListener onPasswordClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int number = (Integer)view.getTag();
            if(passwordCount < 4) {
                passwords += number;
                txtPasswords[passwordCount].setText("*");
                passwordCount++;
                if(passwordCount == 4) {
                    String password = HGSharedPreferences.getInstance(PasswordActivity.this).get("password");
                    switch(mode) {
                        case 2: // 비밀번호 확인
                            if(password.equals(passwords)) { // 비밀번호 일치
                                setResult(RESULT_OK);
                                finish();
                            } else { // 비밀번호가 틀립니다.
                                txtError.setText(R.string.password_invalid_password);
                                clearPassword(false);
                            }
                            break;
                        case 3:
                            if(password.equals(passwords)) { // 비밀번호 일치
                                setResult(RESULT_OK);
                                finish();
                                Intent intent = new Intent(PasswordActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else { // 비밀번호가 틀립니다.
                                txtError.setText(R.string.password_invalid_password);
                                clearPassword(false);
                            }
                    }
                }
            }
        }
    };

    private void clearPassword(boolean clearError) {
        passwordCount = 0;
        passwords = "";
        for(int i=0; i<4; i++) txtPasswords[i].setText("_");
        if(clearError) txtError.setText("");
    }
}
