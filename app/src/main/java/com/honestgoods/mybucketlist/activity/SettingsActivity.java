package com.honestgoods.mybucketlist.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.honestgoods.mybucketlist.Manifest;
import com.honestgoods.mybucketlist.item.BucketItem;
import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.HGUtils;
import com.honestgoods.mybucketlist.R;
import com.honestgoods.mybucketlist.SQliteHelper;
import com.honestgoods.mybucketlist.adapter.NotifySpinnerAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, Spinner.OnItemSelectedListener, View.OnClickListener {
    public static final int RQ_PASSWORD_ENABLE = 0;
    public static final int RQ_PASSWORD_CHAGNE = 1;
    public static final int RQ_PASSWORD_UNABLE = 2;
    public static final int RQ_SAVE_DATA = 3;
    public static final int RQ_LOAD_DATA = 4;
    public static final int RQ_SELECT_FILE = 5;

    private Switch switchNotify, switchLock;
    private RadioButton radioNotifyRandom, radioNotifySelected;
    private Spinner spinnerNotify;
    private Button buttonChangePassword;
    private TextView txtNotifySelect, txtChangePassword;
    private EditText[] editCategoryNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getSupportActionBar().setTitle(R.string.activity_settings_title);

        switchNotify = (Switch)findViewById(R.id.settings_show_notify);
        switchLock = (Switch)findViewById(R.id.settings_toggle_lock);
        radioNotifyRandom = (RadioButton)findViewById(R.id.settings_radio_notify_random);
        radioNotifySelected = (RadioButton)findViewById(R.id.settings_radio_notify_selected);
        spinnerNotify = (Spinner)findViewById(R.id.settings_spinner_notify);
        buttonChangePassword = (Button)findViewById(R.id.settings_button_change_password);
        txtNotifySelect = (TextView)findViewById(R.id.text_notify_select);
        txtChangePassword = (TextView)findViewById(R.id.text_chagne_password);

        // Notify 설정
        HGSharedPreferences pref = HGSharedPreferences.getInstance(this);
        boolean enableNotify = pref.get("settings_notify").equals("true");
        switchNotify.setChecked(enableNotify);
        switchNotify.setOnCheckedChangeListener(this);

        radioNotifyRandom.setEnabled(enableNotify);
        radioNotifySelected.setEnabled(enableNotify);

        boolean notifyRandom = pref.get("settings_notify_option").equals("random");
        if(notifyRandom) radioNotifyRandom.setChecked(true);
        else radioNotifySelected.setChecked(true);
        enableNotifySelect(enableNotify && !notifyRandom);

        radioNotifyRandom.setOnCheckedChangeListener(this);
        radioNotifySelected.setOnCheckedChangeListener(this);

        NotifySpinnerAdapter adapter = new NotifySpinnerAdapter(this, SQliteHelper.getInstance(this).selectAll());
        spinnerNotify.setAdapter(adapter);
        int dataSize = adapter.getCount();
        int selectedNumber = pref.getNotifySelected();
        for(int i=0; i<dataSize; i++) {
            BucketItem item = (BucketItem)adapter.getItem(i);
            if(item.getNum() == selectedNumber) {
                spinnerNotify.setSelection(i);
                break;
            }
        }
        spinnerNotify.setOnItemSelectedListener(this);

        // Category 설정
        editCategoryNames = new EditText[3];
        editCategoryNames[0] = (EditText)findViewById(R.id.settings_category_name_1);
        editCategoryNames[1] = (EditText)findViewById(R.id.settings_category_name_2);
        editCategoryNames[2] = (EditText)findViewById(R.id.settings_category_name_3);

        String[] names = HGSharedPreferences.getInstance(this).getCategoryNames(3);
        for(int i=0; i<3; i++) {
            editCategoryNames[i].setText(names[i]);
            editCategoryNames[i].addTextChangedListener(twCategoryNames);
        }

        // Security 설정
        boolean enableLock = pref.get("settings_lock").equals("true");
        switchLock.setChecked(enableLock);
        switchLock.setOnClickListener(this);
        buttonChangePassword.setOnClickListener(this);
        enableChangePassword(enableLock);

        // Data 설정
        findViewById(R.id.button_save_data).setOnClickListener(this);
        findViewById(R.id.button_load_data).setOnClickListener(this);
    }

    private TextWatcher twCategoryNames = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String[] names = new String[3];
            for(int i=0; i<3; i++)
                names[i] = editCategoryNames[i].getText().toString();
            HGSharedPreferences.getInstance(SettingsActivity.this).putCategoryNames(3, names);
        }
    };

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.settings_button_change_password:
                {
                    Intent intent = new Intent(this, PasswordActivity.class);
                    intent.putExtra("mode", 1);
                    startActivityForResult(intent, RQ_PASSWORD_CHAGNE);
                }
                break;
            case R.id.settings_toggle_lock:
                boolean checked = switchLock.isChecked();
                if(checked) {
                    Intent intent = new Intent(this, PasswordActivity.class);
                    intent.putExtra("mode", 0);
                    startActivityForResult(intent, RQ_PASSWORD_ENABLE);
                } else {
                    Intent intent = new Intent(this, PasswordActivity.class);
                    intent.putExtra("mode", 2);
                    startActivityForResult(intent, RQ_PASSWORD_UNABLE);
                }
                break;
            case R.id.button_save_data:
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                    ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RQ_SAVE_DATA);
                } else {
                    saveData();
                }
                break;
            case R.id.button_load_data:
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
                    ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RQ_LOAD_DATA);
                } else {
                    requestDataFile();
                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(spinnerNotify.isEnabled()) {
            NotifySpinnerAdapter adapter = (NotifySpinnerAdapter) adapterView.getAdapter();
            BucketItem item = (BucketItem) adapter.getItem(i);
            HGUtils.showNotification(this, item);
            HGSharedPreferences.getInstance(this).put("settings_notify_selected", item.getNum() + "");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
        HGUtils.log("onCheckedChanged");
        HGSharedPreferences pref = HGSharedPreferences.getInstance(this);
        switch(compoundButton.getId()) {
            case R.id.settings_show_notify:
                if(checked) {
                    pref.put("settings_notify", "true");
                    if(radioNotifyRandom.isChecked()) {
                        HGUtils.showNotification(this, SQliteHelper.getInstance(this).selectRandom());
                    } else {
                        BucketItem item = (BucketItem)spinnerNotify.getSelectedItem();
                        HGUtils.showNotification(this, item);
                    }
                } else {
                    HGUtils.cancelNotification(this);
                    pref.put("settings_notify", "false");
                }
                radioNotifyRandom.setEnabled(checked);
                radioNotifySelected.setEnabled(checked);
                spinnerNotify.setEnabled(checked);
                break;
            case R.id.settings_radio_notify_random:
                if(checked) {
                    pref.put("settings_notify_option", "random");
                    radioNotifySelected.setChecked(false);
                    HGUtils.showNotification(this, SQliteHelper.getInstance(this).selectRandom());
                }
                break;
            case R.id.settings_radio_notify_selected:
                if(checked) {
                    pref.put("settings_notify_option", "selected");
                    radioNotifyRandom.setChecked(false);
                    BucketItem item = (BucketItem)spinnerNotify.getSelectedItem();
                    HGUtils.showNotification(this, item);
                    pref.putNotifySelected(item.getNum());
                }
                spinnerNotify.setEnabled(checked);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        HGSharedPreferences pref = HGSharedPreferences.getInstance(this);
        switch(requestCode) {
            case RQ_PASSWORD_ENABLE:
                if(resultCode == RESULT_OK) {
                    switchLock.setChecked(true);
                    enableChangePassword(true);
                    pref.put("settings_lock", "true");
                } else {
                    switchLock.setChecked(false);
                }
                break;
            case RQ_PASSWORD_CHAGNE:
                break;
            case RQ_PASSWORD_UNABLE:
                if(resultCode == RESULT_OK) {
                    switchLock.setChecked(false);
                    enableChangePassword(false);
                    pref.put("settings_lock", "false");
                } else {
                    switchLock.setChecked(true);
                }
                break;
            case RQ_SELECT_FILE:
                loadData(data.getData());
                break;
        }
    }

    private void enableNotifySelect(boolean enable) {
        spinnerNotify.setEnabled(enable);
        txtNotifySelect.setEnabled(enable);
    }

    private void enableChangePassword(boolean enable) {
        buttonChangePassword.setEnabled(enable);
        txtChangePassword.setEnabled(enable);
    }

    private void saveData() {
        final String inFileName = "/data/data/" + getPackageName() + "/databases/" + SQliteHelper.databaseName;
        final String outFileName = Environment.getExternalStorageDirectory() + "/MyBucketList.db";
        File dbFile = new File(inFileName);
        try {
            FileInputStream fis = new FileInputStream(dbFile);
            OutputStream os = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length;
            while ((length = fis.read(buff)) > 0) {
                os.write(buff, 0, length);
            }

            os.close();
            fis.close();

            Toast.makeText(this, getString(R.string.message_data_save_success, outFileName), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.message_save_fail, Toast.LENGTH_LONG).show();
        }
    }

    private void requestDataFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, RQ_SELECT_FILE);
    }

    private void loadData(Uri uri) {
        final String outFileName = "/data/data/" + getPackageName() + "/databases/" + SQliteHelper.databaseName;
        try {
            InputStream is = getContentResolver().openInputStream(uri);
            OutputStream os = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length;
            while ((length = is.read(buff)) > 0) {
                os.write(buff, 0, length);
            }

            os.close();
            is.close();

            Toast.makeText(this, R.string.message_load_success, Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.message_load_fail, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == RQ_SAVE_DATA) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                saveData();
            }
        } else if(requestCode == RQ_LOAD_DATA) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                requestDataFile();
            }
        }
    }
}
