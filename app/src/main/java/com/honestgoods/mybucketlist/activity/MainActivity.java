package com.honestgoods.mybucketlist.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.honestgoods.mybucketlist.item.BucketItem;
import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.HGUtils;
import com.honestgoods.mybucketlist.HongAnimationUtil;
import com.honestgoods.mybucketlist.adapter.CategorySpinnerAdapter;
import com.honestgoods.mybucketlist.adapter.MainPagerAdapter;
import com.honestgoods.mybucketlist.R;
import com.honestgoods.mybucketlist.SQliteHelper;
import com.honestgoods.mybucketlist.dialog.SelectStickerDialog;
import com.honestgoods.mybucketlist.dialog.SimpleInputDialog;
import com.honestgoods.mybucketlist.StickerManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private FloatingActionButton fabCreate;
    private ViewPager viewPager;
    private View viewAdd;
    private BucketItem editingItem;
    private AdView adViewBanner;
    private int category = 0;
    private int showMode = 0;
    private EditText editTitle, editGrade, editLimit;
    private ImageView imgSticker;
    private TextView[] txtCategorys;
    private Spinner spinnerCategory;
    private CategorySpinnerAdapter spinnerAdapterCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adViewBanner = (AdView)findViewById(R.id.main_adview);

        getSupportActionBar().setTitle(R.string.activity_main_title);

        fabCreate = (FloatingActionButton)findViewById(R.id.main_fab_create);
        fabCreate.setOnClickListener(this);
        viewAdd = findViewById(R.id.main_view_add);

        viewPager = (ViewPager)findViewById(R.id.main_view_pager);
        viewPager.setAdapter(new MainPagerAdapter(this, fabCreate, 0));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                changeCategory(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        editTitle = (EditText)viewAdd.findViewById(R.id.add_title);
        editGrade = (EditText)viewAdd.findViewById(R.id.add_grade);
        editLimit = (EditText)viewAdd.findViewById(R.id.add_detail);
        imgSticker = (ImageView)viewAdd.findViewById(R.id.add_sticker);
        imgSticker.setTag(Integer.valueOf(-1));
        viewAdd.findViewById(R.id.add_select_sticker).setOnClickListener(this);
        viewAdd.findViewById(R.id.add_submit).setOnClickListener(this);
        viewAdd.findViewById(R.id.add_cancel).setOnClickListener(this);
        setImportanceController();

        editGrade.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().equals("")) return;
                else if(editable.toString().contains(".")) {
                    String preStr = editable.toString().replace(".", "");
                    editable.clear();
                    editable.append(preStr);
                }
                else {
                    if(Integer.parseInt(editable.toString()) > 100) {
                        editable.delete(2,3);
                    }
                    if(editable.toString().length() > 1 && editable.charAt(0) == '0') {
                        editable.delete(0,1);
                    }
                }
            }
        });

        HGSharedPreferences pref = HGSharedPreferences.getInstance(this);
        if(pref.get("settings_notify").equals("true")) {
            if(pref.get("settings_notify_option").equals("random"))
                HGUtils.showNotification(this, SQliteHelper.getInstance(this).selectRandom());
            else {
                int selected = pref.getNotifySelected();
                if(selected != -1) HGUtils.showNotification(this, SQliteHelper.getInstance(this).selectNum(selected));
            }
        }

        txtCategorys = new TextView[3];
        txtCategorys[0] = (TextView)findViewById(R.id.main_category_1);
        txtCategorys[1] = (TextView)findViewById(R.id.main_category_2);
        txtCategorys[2] = (TextView)findViewById(R.id.main_category_3);
        String[] names = pref.getCategoryNames(3);

        for(int i=0; i<3; i++) {
            txtCategorys[i].setText(names[i]);
            txtCategorys[i].setOnClickListener(onCategoryClickListener);
        }
        spinnerCategory = (Spinner)findViewById(R.id.add_category);

        // 구입 아이템 확인
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    private IInAppBillingService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            getPurchases();
        }
    };

    private void getPurchases() {
        HGSharedPreferences pref = HGSharedPreferences.getInstance(this);
        pref.setNoAds(false);
        try {
            Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);
            int response = ownedItems.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String> ownedSkus =
                        ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
//                ArrayList<String>  purchaseDataList =
//                        ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
//                ArrayList<String>  signatureList =
//                        ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
//                String continuationToken =
//                        ownedItems.getString("INAPP_CONTINUATION_TOKEN");
                if (ownedSkus == null) return;
                for (int i = 0; i < ownedSkus.size(); ++i) {
                    if (ownedSkus.get(i).equals("upgrade.remove_ads")) {
                        pref.setNoAds(true);
                    }
                }
                // if continuationToken != null, call getPurchases again
                // and pass in the token to retrieve more items
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if(pref.haveNoAds()) {
            adViewBanner.setVisibility(View.GONE);
        } else {
            MobileAds.initialize(this, getString(R.string.admob_app_id));
            showBannerAd();
        }
    }

    View.OnClickListener onCategoryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            for(int i=0; i<3; i++) {
                if(view.equals(txtCategorys[i])) {
                    viewPager.setCurrentItem(i);
                    break;
                }
            }
        }
    };

    private void changeCategory(int selected) {
        if(selected == category) return;
        txtCategorys[selected].animate().translationY(0);
        txtCategorys[category].animate().translationY((int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics()));
        category = selected;
    }

    private void setImportanceController() {
        View.OnClickListener importanceControlListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = editGrade.getText().toString();
                int importance = 0;
                if(!text.equals("")) importance = Integer.parseInt(text);
                if(view.getId() == R.id.add_importance_up) {
                    if(importance < 100) importance++;
                } else {
                    if(importance > 0) importance--;
                }
                editGrade.setText(importance + "");
            }
        };
        viewAdd.findViewById(R.id.add_importance_up).setOnClickListener(importanceControlListener);
        viewAdd.findViewById(R.id.add_importance_down).setOnClickListener(importanceControlListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_setting:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_show_all:
                item.setChecked(true);
                showMode = 0;
                updateList();
                break;
            case R.id.menu_show_proceed:
                item.setChecked(true);
                showMode = 1;
                updateList();
                break;
            case R.id.menu_show_complete:
                item.setChecked(true);
                showMode = 2;
                updateList();
                break;
            case R.id.menu_upgrade:
                Intent intentUpgrade = new Intent(this, UpgradeActivity.class);
                startActivity(intentUpgrade);
                break;
            case R.id.menu_send_feedback:
                Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                marketLaunch.setData(Uri.parse("market://details?id=com.honestgoods.mybucketlist"));
                startActivity(marketLaunch);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showBannerAd() {
        adViewBanner.setVisibility(View.VISIBLE);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("5174D811535887316CA78B671966851D")
                .build();
        adViewBanner.loadAd(adRequest);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.main_fab_create:
                hideList();
                break;
            case R.id.add_submit:
                submit();
                showList();
                break;
            case R.id.add_cancel:
                showList();
                break;
            case R.id.add_select_sticker:
                int defaultIndex = -1;
                if(imgSticker.getTag() != null)
                    defaultIndex = (int)imgSticker.getTag();
                SelectStickerDialog dialog = new SelectStickerDialog(this, defaultIndex, new SelectStickerDialog.OnCompleteListener() {
                    @Override
                    public void onComplete(SelectStickerDialog dialog, int selected) {
                        dialog.dismiss();
                        imgSticker.setTag(Integer.valueOf(selected));
                        if(selected == -1) {
                            imgSticker.setImageResource(0);
                        }
                        else {
                            imgSticker.setImageResource(StickerManager.stickerArray[selected]);
                        }
                    }
                });
                dialog.show();
                break;
        }
    }

    private void hideList() {
        fabCreate.hide();
        HongAnimationUtil.disappearView(viewPager);
        HongAnimationUtil.collapseView(findViewById(R.id.main_tab), 300, 0, true, null);
        HongAnimationUtil.expandView(viewAdd, 300, getWindow().getAttributes().width, 0);
        String[] names = HGSharedPreferences.getInstance(this).getCategoryNames(3);
        spinnerAdapterCategory = new CategorySpinnerAdapter(this, names);
        spinnerCategory.setAdapter(spinnerAdapterCategory);
        spinnerCategory.setSelection(category);
    }

    public void hideList(BucketItem item) {
        hideList();
        if(item == null) return;
        editingItem = item;
        fillAddView(item);
    }

    private void fillAddView(BucketItem item) {
        editTitle.setText(item.getTitle());
        editGrade.setText(item.getGrade());
        editLimit.setText(item.getLimit());
        imgSticker.setTag(Integer.valueOf(item.getSticker()));
        if(item.getSticker()!=-1) imgSticker.setImageResource(StickerManager.stickerArray[item.getSticker()]);
    }

    private void showList() {
        hideSofyKeyboard();
        HongAnimationUtil.collapseView(viewAdd, 300, 0, true, null);
        HongAnimationUtil.appearView(viewPager);
        HongAnimationUtil.expandView(findViewById(R.id.main_tab), 300, 40, 0);
        fabCreate.show();

        editTitle.setText("");
        editGrade.setText("");
        editLimit.setText("");
        imgSticker.setImageResource(0);
        imgSticker.setTag(Integer.valueOf(-1));
    }

    private void submit() {
        Integer stickerNumber = (Integer)imgSticker.getTag();

        if(editingItem == null) {
            BucketItem item = new BucketItem();
            item.setTitle(editTitle.getText().toString());
            if(editGrade.getText().toString().equals("")) item.setGrade("0");
            else item.setGrade(editGrade.getText().toString());
            item.setLimit(editLimit.getText().toString());
            item.setSticker(stickerNumber);
            item.setCategory(spinnerCategory.getSelectedItemPosition());

            SQliteHelper.getInstance(this).insert(item);
        } else {
            editingItem.setTitle(editTitle.getText().toString());
            if(editGrade.getText().toString().equals("")) editingItem.setGrade("0");
            else editingItem.setGrade(editGrade.getText().toString());
            editingItem.setGrade(editGrade.getText().toString());
            editingItem.setLimit(editLimit.getText().toString());
            editingItem.setSticker(stickerNumber);
            editingItem.setCategory(spinnerCategory.getSelectedItemPosition());

            SQliteHelper.getInstance(this).update(editingItem);
            editingItem = null;
        }
        updateList();
        viewPager.setCurrentItem(category);
    }

    private void updateList() {
        viewPager.setAdapter(new MainPagerAdapter(this, fabCreate, showMode));
    }

    private void hideSofyKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        if(viewPager.getVisibility() == View.GONE) {
            showList();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] names = HGSharedPreferences.getInstance(this).getCategoryNames(3);
        for(int i=0; i<3; i++)
            txtCategorys[i].setText(names[i]);
        updateList();
    }
}
