package com.honestgoods.mybucketlist.activity;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.R;
import com.honestgoods.mybucketlist.item.InAppItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UpgradeActivity extends AppCompatActivity implements View.OnClickListener {
    private ArrayList<InAppItem> inAppItems;
    private Button[] btnBuys;
    private IInAppBillingService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            getInAppList();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade);
        getSupportActionBar().setTitle(R.string.upgrade_activity_title);
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        HGSharedPreferences pref = HGSharedPreferences.getInstance(this);
        btnBuys = new Button[1];
        btnBuys[0] = (Button)findViewById(R.id.upgrade_buy_remove_ads);
        if(pref.haveNoAds()) {
            btnBuys[0].setText(R.string.upgrade_purchased);
            btnBuys[0].setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        InAppItem item = (InAppItem)view.getTag();
        buyItem(item.getProductId());
    }

    private void getInAppList() {
        ArrayList<String> skuList = new ArrayList<String>();
        skuList.add("upgrade.remove_ads");
        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
        try {
            Bundle skuDetails = mService.getSkuDetails(3, getPackageName(), "inapp", querySkus);
            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");

                inAppItems = new ArrayList<>();
                for (int i=0; i<responseList.size(); i++) {
                    JSONObject object = new JSONObject(responseList.get(i));
                    InAppItem item = new InAppItem();
                    item.setProductId(object.getString("productId"));
                    item.setPrice(object.getString("price"));
                    item.setPurchased(false);
                    inAppItems.add(item);
                    btnBuys[i].setText(item.getPrice());
                    btnBuys[i].setTag(item);
                    btnBuys[i].setOnClickListener(this);
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void buyItem(String itemId) {
        try {
            Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(), itemId, "inapp", "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
            startIntentSenderForResult(pendingIntent.getIntentSender(),
                    1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                    Integer.valueOf(0));
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Toast.makeText(this, "구매 오류", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
//            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
//            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    for(int i=0; i<inAppItems.size(); i++) {
                        if(sku.equals(inAppItems.get(i).getProductId())) {
                            btnBuys[i].setText(R.string.upgrade_purchased);
                            btnBuys[i].setEnabled(false);
                            if(i==0) HGSharedPreferences.getInstance(this).setNoAds(true);
                            break;
                        }
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }
}
