package com.honestgoods.mybucketlist.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.honestgoods.mybucketlist.HGSharedPreferences;
import com.honestgoods.mybucketlist.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(HGSharedPreferences.getInstance(SplashActivity.this).get("settings_lock").equals("true")) {
                    Intent intent = new Intent(SplashActivity.this, PasswordActivity.class);
                    intent.putExtra("mode", 3);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
