package com.honestgoods.mybucketlist.item;

/**
 * Created by Hong Tae Joon on 2017-03-02.
 */

public class InAppItem {
    private String productId;
    private String price;
    private boolean isPurchased;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isPurchased() {
        return isPurchased;
    }

    public void setPurchased(boolean purchased) {
        isPurchased = purchased;
    }
}
