package com.honestgoods.mybucketlist;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Hong on 2016-12-29.
 */

public class HGSharedPreferences {
    private static HGSharedPreferences instance;
    private Context context;
    public static HGSharedPreferences getInstance(Context context) {
        if(instance == null) instance = new HGSharedPreferences(context);
        return instance;
    }

    public HGSharedPreferences(Context context) {
        this.context = context;
    }

    public void put(String key, String data) {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, data);
        editor.apply();
    }

    public void putNotifySelected(int number) {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("settings_notify_selected", number);
        editor.apply();
    }

    public int getNotifySelected() {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        try {
            return pref.getInt("settings_notify_selected", -1);
        } catch(ClassCastException e) {
            return -1;
        }
    }

    public void putCategoryNames(int size, String[] names) {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        SharedPreferences.Editor editor = pref.edit();
        for(int i=0; i<size; i++) {
            editor.putString("settings_category_name_" + i, names[i]);
        }
        editor.apply();
    }

    public String[] getCategoryNames(int size) {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        String[] names = new String[size];
        for(int i=0; i<size; i++) {
            names[i] = pref.getString("settings_category_name_" + i, context.getString(R.string.settings_category_default, i+1));
        }
        return names;
    }

    public String get(String key) {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        return pref.getString(key, "");
    }

    public boolean haveNoAds() {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        return pref.getBoolean("noAds", false);
    }

    public void setNoAds(boolean noAds) {
        SharedPreferences pref = context.getSharedPreferences("MyBucketList", 0);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean("noAds", noAds);
        edit.apply();
    }
}
