* 1.5.0
- 중요도를 조절할 수 있는 버튼을 추가하였습니다.
- 중요도의 글자 색상을 변경하였습니다.
- 순위를 표시하는 글자를 삭제하였습니다.

* 1.4.8
- 동영상 광고를 제거하고 전면 광고를 삽입하였습니다.

* 1.4.7
- 오류 수정

* 1.4.6
- 새로운 스티커를 사용할 수 있습니다.

* 1.4.5
- 에러 수정

*1.4.4
- 스티커 추가

*1.4.3
- 광고 수정

*1.4.2
- 스티커 관련 에러 수정
- 광고 보기 버튼 추가

*1.4
- 스티커 기능이 추가되었습니다.
- 상태바에 표시할 수 있는 기능이 추가되었습니다.

*1.3.3
- 광고가 표시되지 않는 오류를 수정하였습니다.
- 리스트에서도 적은 확률로 광고가 나타납니다.

*1.3.2
- 일부 기기에서 디자인이 이상한 오류를 수정하였습니다.
- 버킷 리스트를 삭제했을 때 순위가 알맞게 바뀌지 않는 오류를 수정하였습니다.
- 완료된 버킷 리스트만 볼 수 있는 기능이 추가 되었습니다.
- 오류 수정